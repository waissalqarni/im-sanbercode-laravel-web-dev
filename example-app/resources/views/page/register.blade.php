@extends('layouts.master')
@section('judul')
Sign Up Form
@endsection
@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="fname"><br> <br>
        <label>Last Name</label> <br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other <br> <br>
        <label> Nationality :</label> <br> <br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">Korean</option>
            <option value="">Indian</option>
        </select> <br> <br>
        <label > Language Spoken</label> <br> <br>
        <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa"> English <br>
        <input type="checkbox" name="bahasa"> Other <br> <br>

        <label > Bio</label> <br>
        <textarea cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
