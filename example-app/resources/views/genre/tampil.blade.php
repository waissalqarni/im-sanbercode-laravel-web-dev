@extends('layouts.master')
@section('judul')
HALAMAN TAMPIL GENRE
@endsection
@section('content')

<a href="/category/create" class="btn btn-primary btn-sm"> Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key=>$item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
                <td>

                <form action="/category/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/category/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                   <a href="/category/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="DELETE" >


                </form>
            </td>

          </tr>
        @empty
        <p>No users</p>
        @endforelse

    </tbody>
  </table>
@endsection



