@extends('layouts.master')
@section('judul')
HALAMAN TAMBAH Genre
@endsection
@section('content')
<h1>{{$category->nama}}</h1>

<div class="row">
@forelse ( $category->ListFilm as $item)
<div class="col-4">
    <div class="card" >
        <img src="{{asset('poster/'. $item->poster)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <h3>{{$item->judul}}</h3>
          <p class="card-text">{{Str::limit($item->ringkasan, 60)}}</p>
          <h5 class="card-text">{{$item->tahun}}</h5>
          <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
        </div>
    </div>
</div>

@empty
    <h4>Tidak ada Film</h4>
@endforelse
</div>
<a href="/category" class="btn btn-secondary btn-sm">Kembali</a>
@endsection

