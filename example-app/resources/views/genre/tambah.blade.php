@extends('layouts.master')
@section('judul')
HALAMAN TAMBAH GENRE
@endsection
@section('content')
<form action="/category" method="POST">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    @csrf
    <div class="form-group">
      <label for="name">Nama </label>
      <input type="text" name="nama" class="form-control" >
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
  @endsection


