@extends('layouts.master')
@section('judul')
HALAMAN DETAIL FILM
@endsection
@section('content')

<img src="{{asset('poster/'. $film->poster)}}" width="90%" height="400px"alt="">
<h1 class="text-primary">{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>

<hr>
<h4>List Review</h4>
@forelse ($film->Kritik as $item)
<div class="card">
    <div class="card-header">
      {{$item->user->name}}
    </div>
    <div class="card-body">
      <p class="card-text">{{$item->content}}</p>
    </div>
  </div>

@empty
    <h4>Tidak ada Review</h4>
@endforelse
<br>
@auth
<h4>Tambah Review</h4>
</hr>
    <form action="/kritik/{{$film->id}}" method="POST" class="mb-4">
        @csrf
        <textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="Isi Review disini"></textarea><br>
        <input type="submit" value="Submit Review" class="btn btn-block btn-primary">
    </form>
@endauth
<a href="/film" class="btn btn-secondary btn-sm">Kembali</a>
@endsection
