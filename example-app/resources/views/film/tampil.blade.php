@extends('layouts.master')
@section('judul')
HALAMAN TAMPIL FILM
@endsection
@section('content')
@auth
<a href="/film/create" class="btn btn-primary btn-sm my-3 ">Tambah</a>

@endauth

<div class="row">
    @forelse ($film as $item )
    <div class="col-4">
        <div class="card" >
            <img src="{{asset('poster/'. $item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h3>{{$item->judul}}</h3>

              <span class="badge badge-info">{{$item->genre->nama}}</span>
              <p class="card-text">{{Str::limit($item->ringkasan, 60)}}</p>
              <h5 class="card-text">{{$item->tahun}}</h5>
              <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
                @auth
                <div class="row my-2">
                    <div class="col">
                        <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                    </div>

                    <div class="col">
                        <form action="/film/{{$item->id}}" method= "POST">
                            @csrf
                            @method("DELETE")
                        <input type="submit" value="Delete" class="btn btn-danger btn-block">
                        </form>
                    </div>
                  </div>
                @endauth

        </div>
        </div>
    </div>

    @empty
        <h4>Tidak Ada Film</h4>
    @endforelse

</div>

@endsection
