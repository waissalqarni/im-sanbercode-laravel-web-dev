<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store($film_id, Request $request){
        $request -> validate([
            'content' => 'required',
        ]);

        Kritik :: Create([
            'user_id' => Auth::id(),
            'film_id' => $film_id,
            'content' => $request->input('content'),
        ]);

        return redirect ('/film/'.$film_id);
    }
}
