<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use symfony\Component\Console\Input\Input;
use App\Models\Genre;

class GenreController extends Controller
{
    public function create(){
        return view ('genre.tambah');
    }

    public function store(request $request){
        $request -> validate([
            'nama' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' => $request->input('nama'),
        ]);

        return redirect('/category');
    }
    public function index(){
        $genre = DB::table('genre')->get();

        return view('genre.tampil', ['genre' => $genre]);
    }

    public function show($id){
        $category = Genre::find($id);

        return view ('genre.detail', ['category'=> $category]);
    }

    public function edit($id){
        $category = DB::table('genre')->find($id);

        return view ('genre.edit', ['category'=> $category]);
    }

    public function update($id, Request $request){
        $request -> validate([
            'nama' => 'required ',
        ]);

        DB::table('genre')
              ->where('id', $id)
              ->update(
                [
                'nama'=>$request->input('nama'),
            ]);
            return redirect('category');
    }

    public function destroy($id){
        $deleted = DB::table('genre')->where('id', '=', $id)->delete();

        return redirect('/category');

    }
}


