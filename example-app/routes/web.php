<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// alamat url
Route::get('/', [HomeController :: class, 'home']);
Route::get('/register', [AuthController :: class, 'daftar']);

Route::POST('/welcome', [AuthController :: class, 'welcome']);

Route::get('/data-tables', function (){
    return view ('page.data-tables');
});

Route::get('/table', function (){
    return view ('page.table');
});


Route::middleware(['auth'])->group(function () {
    // CRUD genre
//create data genre
Route::get('/category/create', [GenreController::class, 'create']);
Route::POST('/category', [GenreController::class, 'store']); //Menyimpan data category

//Read data genre
Route::get('/category', [GenreController::class, 'index']); //menampilkan semua data genre ke table
Route::get('/category/{id}', [GenreController::class, 'show']); //menampilkan berdasarkan id

//update data
Route::get('/category/{id}/edit', [GenreController::class, 'edit']);//form mengedit data
Route::put('/category/{id}', [GenreController::class, 'update']);//update data berdasarkan id

//delete
Route::delete('/category/{id}', [GenreController::class, 'destroy']);//menghapus data

Route::post('kritik/{film_id}', [KritikController::class, 'store']); // Menampung data review

});


//CRUD FILM
Route::resource('film', FilmController::class);

Auth::routes();

