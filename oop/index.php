<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");


$sheep = new animal("Shaun");

echo "Nama Hewan  : " . $sheep->name . "<br>";
echo "Jumlah Kaki : " . $sheep->legs . "<br>";
echo "Apakah termasuk hewan berdarah dingin ?  : " . $sheep->cold_blooded . "<br> <br>";

$kodok = new frog("Kera Sakti");

echo "Nama Hewan  : " . $kodok->name . "<br>";
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Apakah termasuk hewan berdarah dingin ?  : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump() ."<br> <br>";

$sungkong = new ape("Kera Sakti");

echo "Nama Hewan  : " . $sungkong->name . "<br>";
echo "Jumlah Kaki : " . $sungkong->legs . "<br>";
echo "Apakah termasuk hewan berdarah dingin ?  : " . $sungkong->cold_blooded . "<br>";
echo "Yell : " . $sungkong->yell();
?>