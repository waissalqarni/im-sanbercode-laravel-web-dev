<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Conditional PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";


// Code function di sini
function greetings($nama){
    echo "Halo" . $nama, "Selamat datang di Sanbercode! <br>";
}

// Hapus komentar untuk menjalankan code!
greetings("Bagas") ;
greetings("Wahyu");
greetings("nama peserta");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";


// Code function di sini 
function reverse($kata1){
    $panjangkata = strlen($kata1);
    $tampung = "";
    for($i=($panjangkata - 1);$i>=0; $i --){
        $tampung = $tampung. $kata1[$i];
    }
    return $tampung;
    
}

function reverseString($kata2){
    $string=reverse($kata2);
    echo $string ."<br>";
}

// Hapus komentar di bawah ini untuk jalankan Code
reverseString("nama peserta");
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";

// Code function di sini
function palindrome($katapalin){
    $katabalik=reverse($katapalin);
    if($katabalik === $katapalin){
        echo $katapalin . "- true <br>";
    }else {
        echo $katapalin . " - false <br>";
    }
}
// Hapus komentar di bawah ini untuk jalankan code
    palindrome("civic") ; 
    palindrome("nababan") ; 
    palindrome("jambaban"); 
    palindrome("racecar"); 


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
// Code function di sini
    function tentukan_nilai($nilai){
    if ($nilai >=98 && $nilai <=100){
        return $nilai . "=> sangat baik <br>"; 
    } else if ($nilai >=76 && $nilai < 98){
        return $nilai . "=> baik <br>";
    } else if  ($nilai >=67 && $nilai < 76){
        return $nilai . "=> cukup <br>"; 
    } else  if($nilai >=43 && $nilai < 67){
        return $nilai . "=> kurang <br>"; 
}}
// Hapus komentar di bawah ini untuk jalankan code
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


?>

</body>

</html>