<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping pertama I Love PHP</h3>";
        $i=2;
        while($i<=20){
            echo "$i - I Love PHP <br>";
            $i += 2; 
        }
        echo "<h3>Soal No 1 Looping kedua I Love PHP</h3>"; 
        $a=20;
        do{
            echo "$a - I Love PHP <br>";
            $a -= 2; 
        }
        while($a>=2);  
    
        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
         
             $numbers = [18, 45, 29, 61, 47, 34];
             echo "array numbers: ";
             print_r($numbers);
             echo "<br>";
             echo "array sisa baginya adalah : ";
             foreach($numbers as $value){
                $rest[] =$value % 5; 
             }
             print_r ($rest);
        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        $biodata= [
            ["001", "keyboard logistik" , "60000", "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
            ["002", "keyboard MSI" , "60000", "Keyboard yang mantap untuk kantoran", "logitek.jpeg"] ,
            ["003", "Mouse Genius" , "60000", "Keyboard yang mantap untuk kantoran", "logitek.jpeg"] ,
            ["004", "Mouse jerry" , "60000", "Keyboard yang mantap untuk kantoran", "logitek.jpeg"] ,

        ];
        foreach($biodata as $key => $value){
            $item = array(
            'id' => $value[0],
            'name' => $value[1],
            'price' => $value[2],
            'description' => $value[3],
            'source' => $value[4], 
            );
            print_r($item);
            echo "<br>";
        } ;

        echo "<h3>Soal No 4 Asterix </h3>";
        for($j =1; $j<=5; $j++){
            for ($b=1; $b<=$j; $b++){
                echo " * ";
            }

            echo  "<br>";
        }
        
    ?> 