<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $string = "PHP is never old";
        echo "kalimat 1 : ". $string . "<br>";
        echo "panjang kalimat 1 : " .strlen($string). "<br>";
        echo "Jumlah kata kalimat 1 : " .str_word_count($string). "<br>";
       
        
        echo "<h3> Soal No 2</h3>";
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo " Kata Kedua: ". substr($string2, 2, 5) . "<br>";
        echo " Kata Ketiga: ". substr($string2, 6, 8) . "<br>";

        echo "<h3> Soal No 3 </h3>";
        $string3 = "PHP is old but sexy!";
        echo "String3: ". $string3."<br>"; 
        echo "String 3 ganti : ". str_replace("sexy", "awesome" , $string3);


    ?>
</body>
</html>